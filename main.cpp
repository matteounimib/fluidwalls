#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include <cmath>
#include "include/structure.h"
using namespace std;

int main(int argc, char *argv[]) {
  srand(time(NULL));
  const int maxtime = 1;
  const double delta = 0.01;
  const int L = 15;
  const double rho = 3;
  const double rc = 1;
  double current_temp;
  // x,y: position
  vector<double> X = {};
  vector<double> Y = {};
  vector<double> Z = {};
  // t: type of particle 
  vector<Type> T = {};
  // velocities
  vector<double> vx = {};
  vector<double> vy = {};
  vector<double> vz = {};
  // forces
  vector<double> fx = {};
  vector<double> fy = {};
  vector<double> fz = {};
  map<int, vector<int>> chain_struct = init(X, Y, Z, T, vx, vy, vz,
                                            fx, fy, fz, L, rho);
  // X.push_back(1);
  // Y.push_back(0);
  // X.push_back(L/2);
  // Y.push_back(L/2);
  // X.push_back(-L/2);
  // Y.push_back(-L/2);
  // X.push_back(L/2-1);
  // Y.push_back(L/2-0.6);
  // T.push_back(Type::fluid);
  // T.push_back(Type::fluid);
  // T.push_back(Type::fluid);
  // T.push_back(Type::fluid);
  xyz(X, Y, Z, T, "test.xyz");
  cout << Z.size() << endl;
  // vector<double> output = {};
  // vector<vector<int>> cell_list = make_cell_list(X, Y, 1, L);
  // map<int, vector<double>>::iterator it;
  // for (it = cell_list.begin(); it != cell_list.end(); ++it){
    // cout << it->first << endl;
    // for (int i = 0; i < it->second.size(); ++i){
      // cout << "X " << X[it->second[i]] << endl;
      // cout << "Y " << Y[it->second[i]] << endl;
  //   }
  // }
  // for (int i = 0; i < nei.size(); ++i){
    // cout << "n x " << X[nei[i]] << endl;
    // cout << "n y " << Y[nei[i]] << endl;
  // }
  int p = 0;
  vector<vector<int>> cell_list = make_cell_list(X, Y, Z, 1, L);
  for (double i = 0; i < maxtime; i += delta) {
    integrate(X, Y, Z, T, vx, vy, vz, fx, fy, fz, delta, L, cell_list, chain_struct);
    // if (p % 3 == 0)
      xyz(X, Y, Z, T, "test.xyz");
    cout << i << endl;
    p++;
    // current_temp = total_temperature(vx, vy);
    // cout << current_temp << endl;
  }
  }

