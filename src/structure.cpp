#include "../include/structure.h"
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <math.h>
#include <numeric>
#include <ostream>
#include <random>
#include <type_traits>
#include <utility>
#include <vector>
using namespace std;

map<vector<Type>, double> interactions = {
    {{Type::a, Type::a}, 50},         {{Type::a, Type::b}, 25},
    {{Type::a, Type::fluid}, 25},     {{Type::a, Type::wall}, 200},

    {{Type::b, Type::a}, 25},         {{Type::b, Type::b}, 1},
    {{Type::b, Type::fluid}, 300},    {{Type::b, Type::wall}, 200},

    {{Type::fluid, Type::a}, 25},     {{Type::fluid, Type::b}, 300},
    {{Type::fluid, Type::fluid}, 25}, {{Type::fluid, Type::wall}, 200},

    {{Type::wall, Type::a}, 200},     {{Type::wall, Type::b}, 200},
    {{Type::wall, Type::fluid}, 200}, {{Type::wall, Type::wall}, 0}};
std::ostream &operator<<(std::ostream &strm, Type t) {
  const std::string name[] = {"w", "f", "a", "b"};
  return strm << name[t];
}

double count(vector<double> &V) {
  double result = 0;

  for (int i = 0; i < V.size(); ++i) {
    // cout << result << endl;
    result += V[i];
  }
  return result;
}

double omega(double r, double rc) {
  if (r <= rc)
    return 1 - r / rc;
  else
    return 0;
}
vector<double> r_hat(double dis, vector<double> r) {
  vector<double> result = {};
  for (int i = 0; i < r.size(); ++i) {
    // cout << "ri " << r[i] << endl << "dis "<<  dis << endl <<
    //   "ri / dis " << r[i] / dis << endl;
    result.push_back(r[i] / dis);
  }
  return result;
}

vector<double> Fc(double dis, vector<double> r, vector<double> rh, double a,
                  double rc) {
  vector<double> result = {};
  if (dis >= rc) {
    return {0, 0, 0};
  } else {
    for (int i = 0; i < rh.size(); ++i) {
      // cout << a * (1 - dis / rc) * rhat[i] << endl;
      result.push_back(a * (1 - dis / rc) * rh[i]);
    }
    return result;
  }
}

// assume gamma = 4.5
vector<double> Fd(double dis, vector<double> r, vector<double> rh,
                  vector<double> v, double rc) {
  double gamma = 4.5;
  vector<double> result = {};
  //  double r = eu_distance(x1, y1, x2, y2, L);
  double w = pow(omega(dis, rc), 2);
  // double v = vx1 - vx2 + vy1 - vy2;
  double prod = inner_product(rh.begin(), rh.end(), v.begin(), 0);
  for (int i = 0; i < r.size(); ++i) {
    result.push_back(-gamma * w * dis * prod * rh[i]);
  }
  return result;
}

// assume sigma = 1
vector<double> Fr(double dis, vector<double> r, vector<double> rh, double rc) {
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::normal_distribution<double> norm{0, 1};
  double sigma = 1;
  vector<double> result = {};
  double w = omega(dis, rc);
  // cout << "w " << w << endl;
  double xi = norm(gen);
  for (int i = 0; i < r.size(); ++i) {
    result.push_back(sigma * w * xi * rh[i]);
  }
  return result;
}

// total force
vector<double> Ft(double dis, vector<double> r, vector<double> v, double a,
                  double rc) {

  vector<double> rh = r_hat(dis, r);
  vector<double> fc = Fc(dis, r, rh, a, rc);
  vector<double> fd = Fd(dis, r, rh, v, rc);
  vector<double> fr = Fr(dis, r, rh, rc);
  vector<double> result = {};
  for (int i = 0; i < fc.size(); ++i) {
    result.push_back(fc[i] + fd[i] + fr[i]);
  }
  // testing that is a unit vector
  // cout << sqrt(rh[0] * rh[0] + rh[1] * rh[1])<< endl;
  return result;
}
// bond force
vector<double> Fs(double dis, vector<double> r, vector<double> rh, double Ks,
                  double rs) {
  vector<double> result = {};
  for (int i = 0; i < rh.size(); ++i) {
    result.push_back(Ks * (1 - dis / rs) * rh[i]);
  }
  // cout << sqrt(rh[0]*rh[0] + rh[1]*rh[1]) << endl;
  return result;
}

vector<vector<int>> make_cell_list(vector<double> &X, vector<double> &Y,
                                   vector<double> &Z, double rc, double L) {
  vector<vector<int>> result;
  double xmin = -L / 2;
  // int?
  int posx = 0;
  int posy = 0;
  int posz = 0;
  int pos = 0;
  // initialize the data structure adding the cell IDs
  for (int i = 0; i < L * L * L; i += rc) {
    result.push_back({});
  }
  // add the parciles
  for (int i = 0; i < X.size(); ++i) {
    posx = floor((X[i] - xmin) / rc);
    posy = floor((Y[i] - xmin) / rc);
    posz = floor((Z[i] - xmin) / rc);
    pos = (posz + posy * L + posx * L * L);
    // cout << "X: " << X[i] << " Y: " << Y[i] << endl
    //      << "px " << posx << " py " << posy << endl;
    // cout << pos << endl;
    result[pos].push_back(i);
  }
  return result;
}
void update_cell(vector<vector<int>> &cl, int i, double old_x, double old_y,
                 double old_z, double new_x, double new_y, double new_z,
                 double rc, double xmin, double L) {
  // TODO: check
  int posx = floor((old_x - xmin) / rc);
  int posy = floor((old_y - xmin) / rc);
  int posz = floor((old_z - xmin) / rc);
  int cell_index = (posz + posy * L + posx * L * L);
  cl[cell_index].erase(
      std::remove(cl[cell_index].begin(), cl[cell_index].end(), i),
      cl[cell_index].end());
  posx = floor((new_x - xmin) / rc);
  posy = floor((new_y - xmin) / rc);
  posz = floor((new_z - xmin) / rc);
  cell_index = (posz + posy * L + posx * L * L);
  cl[cell_index].push_back(i);
}

// return neighbour cells given the cell index
vector<int> nei_cells(int cell_index, double rc, int L) {
  vector<int> result = {};
  double xmin = -L / 2;
  // get the adj cells
  // get i j coordinates of the given cell
  int cell_x = floor(cell_index / (L * L));
  int cell_y = floor((cell_index - cell_x * L * L) / L);
  int cell_z = cell_index - cell_y * L - cell_x * L * L;
  // cout << "cell index " << cell_index << endl << "x " << cell_x << endl
  //      <<"y " << cell_y << endl << "z " << cell_z << endl;
  int tmpx = 0;
  int tmpy = 0;
  int tmpz = 0;
  int target = 0;
  for (int i = cell_x - 1; i <= cell_x + 1; ++i) {
    tmpx = i;
    if (tmpx == -1)
      tmpx = L - 1;
    if (tmpx == L)
      tmpx = 0;
    for (int j = cell_y - 1; j <= cell_y + 1; ++j) {
      tmpy = j;
      if (tmpy == -1)
        tmpy = L - 1;
      if (tmpy == L)
        tmpy = 0;
      for (int e = cell_z - 1; e <= cell_z + 1; ++e) {
        tmpz = e;
        if (tmpz == -1)
          tmpz = L - 1;
        if (tmpz == L)
          tmpz = 0;
        target = tmpz + tmpy * L + tmpx * L * L;
        if (target != cell_index) {
          result.push_back(target);
        }
      }
    }
  }
  return result;
}

// return particles given an index cell
vector<int> getParticles(vector<vector<int>> &cl, int cell_index) {
  return cl[cell_index];
}
vector<int> getParticlesCell(vector<vector<int>> &cl, int cell_index) {
  vector<int> result = {};
  if (cl[cell_index].size() > 0) {
    for (int i = 0; i < cl[cell_index].size(); ++i) {
      result.push_back(cl[cell_index][i]);
    }
  }
  return result;
}
vector<int> getParticlesNei(vector<vector<int>> &cl, int cell_index, int rc,
                            double L) {
  // map<int, vector<int>>::iterator it;
  vector<int> result = {};
  // vector<int> out = {};
  vector<int> nei = nei_cells(cell_index, rc, L);
  // cout << "nei size " << nei.size() << endl;
  result = getParticlesCell(cl, cell_index);
  std::vector<vector<int>>::iterator it;
  // for (int i : nei) {
  //   // cout << i << endl;
  //   it = std::find(cl.begin(), cl.end(), cl[i]);
  //   if (it != cl.end()) {
  //     int index = distance(cl.begin(), it);
  //     for (int j = 0; j < cl[index].size(); ++j) {
  //       result.push_back(cl[index][j]);
  //     }
  //   }
  // }
  for (int i : nei){
    for (int j : cl[i]){
      result.push_back(j);
    }
  }
  return result;
}

void xyz(vector<double> &X, vector<double> &Y, vector<double> &Z,
         vector<Type> &T, const std::string filename) {
  std::ofstream target;
  target.open(filename, std::ios_base::app);
  std::map<Type, std::string> mconv = {
      {Type::wall, "C"}, {Type::fluid, "H"}, {Type::a, "F"}, {Type::b, "O"}};
  target << X.size() << std::endl << std::endl;
  for (int i = 0; i < X.size(); ++i) {
    target << mconv.at(T[i]) << " " << X[i] << " " << Y[i] << " " << Z[i]
           << std::endl;
  }
  target.close();
}

double eu_distance(double x1, double y1, double x2, double y2, double z1,
                   double z2, double L) {
  double xr = move(x2 - x1, L);
  // xr = xr - L * round(xr / L);
  double yr = move(y2 - y1, L);
  double zr = move(z2 - z1, L);
  // yr = yr - L * round(yr / L);
  return sqrt(xr * xr + yr * yr + zr * zr);
  // double xr = x2 - x1;
  // while (xr > L / 2){
  //   xr = xr - L;
  // }
  // while (xr <= -L/2){
  //   xr = xr + L;
  // }
  // double yr = y2 - y1;
  // while (yr > L / 2) {
  //   yr = yr - L;
  // }
  // while (yr <= -L / 2) {
  //   yr = yr + L;
  // }
  // return sqrt(xr * xr + yr * yr);
}

// temperature of the system where each particle has mass = 1 (and K_b = 1)
double total_temperature(vector<double> &vx, vector<double> &vy) {
  double result = 0;
  for (int i = 0; i < vx.size(); ++i) {
    result += 0.5 * (vx[i] * vx[i] + vy[i] * vy[i]);
  }
  return result / vx.size();
}
map<int, vector<int>> chain(int n, vector<double> &X, vector<double> &Y,
                            vector<double> &Z, vector<Type> &T,
                            vector<double> &vx, vector<double> &vy,
                            vector<double> &vz, vector<double> &fx,
                            vector<double> &fy, vector<double> &fz,
                            const double L) {
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::uniform_real_distribution<> dist(-L / 2, L / 2);
  map<int, vector<int>> result;
  // set position of chain head randomly
  double hx = 0;
  double hy = 0;
  double hz = 0;
  int pos = 0;
  for (int i = 0; i < n; ++i) {
    hx = dist(gen);
    hy = dist(gen);
    hz = dist(gen);
    result.insert(std::pair<int, vector<int>>(i, {}));
    for (int j = 0; j < 7; ++j) {
      X.push_back(move((hx + j * 0.05), L));
      Y.push_back(move((hy + j * 0.05), L));
      Z.push_back(move((hz + j * 0.05), L));
      vx.push_back(0);
      vy.push_back(0);
      vz.push_back(0);
      fx.push_back(0);
      fy.push_back(0);
      fz.push_back(0);
      if (j < 2) {
        T.push_back(Type::a);
      } else {
        T.push_back(Type::b);
      }
      result.find(i)->second.push_back(pos);
      pos++;
    }
  }
  return result;
}

vector<double> linspace(double begin, double end, int n) {
  vector<double> result = {};
  double ratio = (end - begin) / n;
  double i = begin;
  while (i < end) {
    result.push_back(i);
    i += ratio;
  }
  return result;
}

//  3d completed using rings on the same plane
map<int, vector<int>> ring(int n, vector<double> &X, vector<double> &Y,
                           vector<double> &Z, vector<Type> &T,
                           vector<double> &vx, vector<double> &vy,
                           vector<double> &vz, vector<double> &fx,
                           vector<double> &fy, vector<double> &fz,
                           const double L) {

  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::uniform_real_distribution<> dist(-L / 2 + 1.4, L / 2 - 1.4);
  map<int, vector<int>> result;
  double radius = 0.5;
  double centerx = 0.0;
  double centery = 0.0;
  double centerz = 0.0;
  vector<double> points = linspace(0, 2 * M_PI, 9);
  // set position of chain head randomly
  int pos = 0;
  for (int i = 0; i < n; ++i) {
    centerx = dist(gen);
    centery = dist(gen);
    centerz = dist(gen);
    result.insert(std::pair<int, vector<int>>(i, {}));
    for (auto j : points) {
      X.push_back(move((centerx + radius * cos(j)), L));
      Y.push_back(move((centery + radius * sin(j)), L));
      Z.push_back(move((centerz + radius), L));
      vx.push_back(0);
      vy.push_back(0);
      vz.push_back(0);
      fx.push_back(0);
      fy.push_back(0);
      fz.push_back(0);
      T.push_back(Type::a);
      result.find(i)->second.push_back(pos);
      pos++;
    }
  }
  return result;
}
map<int, vector<int>> init(vector<double> &X, vector<double> &Y,
                           vector<double> &Z, vector<Type> &T,
                           vector<double> &vx, vector<double> &vy,
                           vector<double> &vz, vector<double> &fx,
                           vector<double> &fy, vector<double> &fz,
                           const double L, const double rho) {
  std::random_device rd{};
  std::mt19937 gen{rd()};
  std::uniform_real_distribution<> dist(-L / 2, L / 2);
  const double wall_size = 1;
  const int chains = 450;
  const int ring_len = 7;
  double N = rho * L * L * L;
  map<int, vector<int>> chain_struct =
      chain(chains, X, Y, Z, T, vx, vy, vz, fx, fy, fz, L);
  // map<int, vector<int>> chain_struct = ring(chains, X, Y, Z, T, vx, vy, vz,
  // fx, fy, fz, L);
  for (int i = ring_len * chains; i < N; ++i) {
    double x_pos = dist(gen);
    X.push_back(x_pos);
    Y.push_back(dist(gen));
    Z.push_back(dist(gen));

    if (x_pos <= wall_size - L / 2 || x_pos > L / 2 - wall_size) {
      T.push_back(Type::wall);
    } else {
      T.push_back(Type::fluid);
    }
    if (T[i] == Type::wall && x_pos <= wall_size - L / 2) {
      vy.push_back(-5);
    } else if (T[i] == Type::wall && x_pos > L / 2 - wall_size) {
      vy.push_back(5);
    } else {
      vy.push_back(0);
    }
    vx.push_back(0);
    vz.push_back(0);

    fx.push_back(0);
    fy.push_back(0);
    fz.push_back(0);
  }
  return chain_struct;
}

// move in periodic boundary conditions
double move(double x, double L) { return x - L * round(x / L); }

vector<double> integrate(vector<double> &X, vector<double> &Y,
                         vector<double> &Z, vector<Type> &T, vector<double> &vx,
                         vector<double> &vy, vector<double> &vz,
                         vector<double> &fx, vector<double> &fy,
                         vector<double> &fz, double delta, double L,
                         vector<vector<int>> &cell_list,
                         map<int, vector<int>> chain_struct) {
  double old_x;
  double old_y;
  double old_z;
  vector<double> fxp = {};
  vector<double> fyp = {};
  vector<double> fzp = {};
  vector<double> vxp = {};
  vector<double> vyp = {};
  vector<double> vzp = {};

  vector<double> result = {};
  for (int i = 0; i < X.size(); ++i) {
    old_x = X[i];
    old_y = Y[i];
    old_z = Z[i];

    if (T[i] != Type::wall) {
      X[i] = move(X[i] + vx[i] * delta + 0.5 * fx[i] * delta * delta, L);
      Y[i] = move(Y[i] + vy[i] * delta + 0.5 * fy[i] * delta * delta, L);
      Z[i] = move(Z[i] + vz[i] * delta + 0.5 * fz[i] * delta * delta, L);
    } else {
      Y[i] = move(Y[i] + vy[i] * delta, L);
    }
    update_cell(cell_list, i, old_x, old_y, old_z, X[i], Y[i], Z[i], 1, -L / 2,
                L);
    vxp = vx;
    vyp = vy;
    vzp = vz;
    if (T[i] != Type::wall) {
      vxp[i] = vx[i] + 0.5 * fx[i] * delta;
      vyp[i] = vy[i] + 0.5 * fy[i] * delta;
      vzp[i] = vz[i] + 0.5 * fz[i] * delta;
    }
  }
  fxp = fx;
  fyp = fy;
  fzp = fz;
  update_forces(X, Y, Z, T, vxp, vyp, vzp, fx, fy, fz, 1, L, cell_list,
                chain_struct);
  for (int i = 0; i < X.size(); ++i) {
    if (T[i] != Type::wall) {
      vx[i] += 0.5 * delta * (fx[i] + fxp[i]);
      vy[i] += 0.5 * delta * (fy[i] + fyp[i]);
      vz[i] += 0.5 * delta * (fz[i] + fzp[i]);
    }
  }
  // cout << "temperature " << total_temperature(vx, vy) << endl;
  // result.push_back(total_temperature(vx, vy));
  // cout  << "vel x: " << count(vx)  << endl;
  // cout << "vel y: " << count(vy) << endl << endl;
  return result;
}

void update_forces(vector<double> &X, vector<double> &Y, vector<double> &Z,
                   vector<Type> &T, vector<double> &vx, vector<double> &vy,
                   vector<double> &vz, vector<double> &fx, vector<double> &fy,
                   vector<double> &fz, double cutoff, double L,
                   vector<vector<int>> &cl,
                   map<int, vector<int>> chain_struct) {

  double r2x = 0.0;
  double r2y = 0.0;
  double r2z = 0.0;
  double xr = 0.0;
  double yr = 0.0;
  double zr = 0.0;
  vector<int> nei = {};
  vector<double> tmp = {0, 0};
  vector<double> tmp_bond = {0, 0};
  double c2 = cutoff * cutoff;
  int tmp_chain = 0;
  double actf = 0;
  vector<double> rhc = {};
  int tot = 0;
  map<int, vector<int>>::iterator it;
  for (int i = 0; i < X.size(); ++i) {
    fx[i] = 0.0;
    fy[i] = 0.0;
    fz[i] = 0.0;
  }
  // without cell list
  // for (int i = 0; i < X.size() -1; ++i) {
  //     // nei = neighbour(cl, i, X, Y, cutoff, L);
  //   for (int j = i + 1; j < X.size(); ++j) {
  // with cell list, fast implementation
  // begin
  vector<int> particles = {};
  vector<int> particlesCell = {};
  vector<vector<int>> tmp_cl = cl;
  for (int c = 0; c < cl.size(); c++) {
    particlesCell = getParticlesCell(cl, c);
    particles = getParticlesNei(tmp_cl, c, cutoff, L);
    // cout << "p cell size " << particles.size() << endl;
    if (particles.size() == 0)
      continue;
    for (int i : particlesCell) {
      for (int j : particles) {
        if (X[i] == X[j] && Y[i] == Y[j] && Z[i] == Z[j])
          continue;
        if (i > j) {
          tot++;
          xr = move(X[i] - X[j], L);
          yr = move(Y[i] - Y[j], L);
          zr = move(Z[i] - Z[j], L);
          r2x = xr * xr;
          r2y = yr * yr;
          r2z = zr * zr;
          r2x = r2x + r2y + r2z;
          if (r2x < c2) {
            r2x = sqrt(r2x);
            actf = interactions.find({T[i], T[j]})->second;

            // if i and j are of type a or b, they can be in the same chain
            tmp_chain = 0;
            if ((T[i] == Type::a || T[i] == Type::b) &&
                (T[j] == Type::a || T[j] == Type::b) &&
                (abs(i - j) == 1 || abs(i - j) == 8)) {
              // search particle ID in all the chains
              for (it = chain_struct.begin(); it != chain_struct.end(); ++it) {
                for (auto m : it->second) {
                  if (m == i) {
                    tmp_chain++;
                  } else if (m == j) {
                    tmp_chain++;
                  }
                }
              }
            }
            if (tmp_chain == 2) {
              rhc = r_hat(r2x, {xr, yr, zr});
              // for chains
              tmp_bond = Fs(r2x, {xr, yr, zr}, rhc, 100, 0.1);
              // for rings
              // tmp_bond = Fs(r2x, {xr, yr}, rhc, 100, 0.3);
              tmp = Ft(r2x, {xr, yr, zr},
                       {vx[i] - vx[j], vy[i] - vy[j], vz[i] - vz[j]}, actf,
                       cutoff);
              // cout << "tot - bond "<< tmp[0]- tmp_bond[0] << endl;
              // cout << "tot " << tmp_bond[0] << endl;
              tmp[0] += tmp_bond[0];
              tmp[1] += tmp_bond[1];
              tmp[2] += tmp_bond[2];

            } else {
              tmp = Ft(r2x, {xr, yr, zr},
                       {vx[i] - vx[j], vy[i] - vy[j], vz[i] - vz[j]}, actf,
                       cutoff);
              // cout << tmp[0] << endl;
            }
            if (T[i] != Type::wall) {
              fx[i] += tmp[0];
              fy[i] += tmp[1] + 0.0;
              fz[i] += tmp[2];
            } else {
              fx[i] += tmp[0];
              fy[i] += tmp[1];
              fz[i] += tmp[2];
            }
            if (T[i] != Type::wall) {
              fx[j] += -tmp[0];
              fy[j] += -tmp[1] + 0.0;
              fz[j] += -tmp[2];
            } else {
              fx[j] += -tmp[0];
              fy[j] += -tmp[1];
              fz[j] += -tmp[2];
            }
            // cout << T[i] << " " << T[j] << " " << i << " " << j << " "
            //      << fx[i] << " " << fx[j] << endl;
            // cout << "x:  " << fx[j] << endl;
            // cout << "y:  " << fy[j] << endl << endl;
            // cout << "N: " << X.size() << endl;
          }
        }
      }
    }
  }
  cout << "tot: " << tot << endl;
}
