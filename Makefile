model: main.o structure.o
	g++ -pg -Ofast main.o structure.o -o model

main.o: main.cpp
	g++ -pg -Ofast -c main.cpp

structure.o: src/structure.cpp include/structure.h
	g++ -pg -Ofast -c src/structure.cpp

clean:
	rm *.o
