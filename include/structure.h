#ifndef MY_DATA_TYPES
#define MY_DATA_TYPES
#include <fstream>
#include <iostream>
#include <map>
#include <type_traits>
#include <unordered_map>
#include <vector>
using namespace std;
enum Type { wall = 0, fluid = 1, a = 2, b = 3 };

std::ostream &operator<<(std::ostream &strm, Type t);

double pos(double i, double j, int size);
template <typename T> void pprint(std::vector<T> V, int dim) {
  for (int i = 0; i < V.size() / dim; ++i) {
    for (int j = 0; j < dim; ++j) {
      std::cout << V[pos(i, j, dim)] << " ";
    }
    std::cout << std::endl;
  }
}
double count(vector<double> &V);
double count_to_set(vector<double> &V);
double eu_distance(double x1, double y1, double x2, double y2, double L);
double temperature(double x, double y);
double total_temperature(vector<double> &vx, vector<double> &vy);

vector<vector<int>> make_cell_list(vector<double> &X, vector<double> &Y,
                                   vector<double> &Z, double rc, double L);
vector<int> nei_cells(int cell_index, double rc, int L);
double omega(double r, double rc);
vector<double> r_hat(double dis, vector<double> r);
vector<double> Fc(double dis, vector<double> r, vector<double> rh, double a,
                  double rc);
vector<double> Fd(double dis, vector<double> r, vector<double> rh,
                  vector<double> v, double rc);
vector<double> Fr(double dis, vector<double> r, vector<double> rh, double rc);
vector<double> Fs(double dis, vector<double> r, vector<double> rh, double Ks,
                  double rs);
vector<double> Ft(double dis, vector<double> r, vector<double> v, double a,
                  double rc);
double move(double x, double L);

vector<double> linspace(double begin, double end, int n);
map<int, vector<int>> init(vector<double> &X, vector<double> &Y,
                           vector<double> &Z, vector<Type> &T,
                           vector<double> &vx, vector<double> &vy,
                           vector<double> &vz, vector<double> &fx,
                           vector<double> &fy, vector<double> &fz,
                           const double L, const double rho);

vector<double> integrate(vector<double> &X, vector<double> &Y,
                         vector<double> &Z, vector<Type> &T, vector<double> &vx,
                         vector<double> &vy, vector<double> &vz,
                         vector<double> &fx, vector<double> &fy,
                         vector<double> &fz, double delta, double L,
                         vector<vector<int>> &cell_list,
                         map<int, vector<int>> chain_struct);

void update_forces(vector<double> &X, vector<double> &Y, vector<double> &Z,
                   vector<Type> &T, vector<double> &vx, vector<double> &vy,
                   vector<double> &vz, vector<double> &fx, vector<double> &fy,
                   vector<double> &fz, double cutoff, double L,
                   vector<vector<int>> &cl, map<int, vector<int>> chain_struct);

void xyz(vector<double> &X, vector<double> &Y, vector<double> &Z,
         vector<Type> &T, const std::string filename);
#endif
